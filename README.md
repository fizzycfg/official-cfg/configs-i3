# i3 Configuration

i3 configuration (fizzy compliant)

## Documentation

See [wiki](https://gitlab.com/fizzycfg/official-cfg/configs-i3/wikis/home)

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md)

## License

See [LICENSE](./LICENSE)
